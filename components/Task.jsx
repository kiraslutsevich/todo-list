import React from 'react';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';


const Task = ({ id, title, deleteTask, navigation }) => {
  return (
    <TouchableOpacity
      onPress={() =>  navigation.navigate('Task', { title })}
      style={styles.textWrapper}
    >
      <View style={{ flexDirection: 'row' }}>
        <View style={{ flex: 1 }}>
          <Text style={{ fontFamily: Platform.OS === 'android' && 'Combo-Regular' }}>
            {title}
          </Text>
        </View>

        <TouchableOpacity
          onPress={() => {
            deleteTask(id)
          }}
        >
          <View>
            <Text> X </Text>
          </View>
        </TouchableOpacity>
      </View>
    </TouchableOpacity>
  )
};

const styles = StyleSheet.create({
  textWrapper: {
    backgroundColor: 'white', 
    paddingVertical: 10, 
    paddingHorizontal: 30, 
    marginBottom: 20,
  },
});

export default Task;