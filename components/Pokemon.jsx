import React from 'react';
import styles from '../styles';
import { Text, View, TouchableHighlight } from 'react-native';

const Pokemon = ({ title, navigation }) => {
    return (
        <View style={styles.styles.task}>
            <TouchableHighlight
                activeOpacity={0.6}
                underlayColor={styles.colors.dark}
                onPress={() => navigation.navigate('Pokemon')}
            >
                <View style={styles.styles.linkText}>
                    <Text style={styles.styles.text}>{title}</Text>
                </View>
            </TouchableHighlight>
        </View>
    )
};

export default Pokemon;