import { StyleSheet } from "react-native";

const colors = {
    light: '#f5e7df',
    dark: '#db8a5c',
    red: '#ff6347',
    black: '#000000',
    focus: '#f5e8af',
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'stretch',
        backgroundColor: '#fff',
    },
    list: {
        flex: 1,
        backgroundColor: colors.dark,
        borderBottomColor: colors.black,
    },
    text: {
        color: colors.red,
        fontFamily: Platform.OS === 'android' && 'Combo-Regular',
        fontSize: 20,
    },
    task: {
        backgroundColor: colors.light,
        padding: 20,
        flex: 1,
        marginVertical: 8,
        marginHorizontal: 16,
        borderBottomColor: colors.black,
        borderRightColor: colors.black,
        borderRightWidth: 3,
        borderBottomWidth: 3,
        borderRadius: 8,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    buttonWrapper: {
        width: 40,
        height: 40,
        flex: 0.2,
        borderRadius: 3,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.dark,
    },
    buttonText: {
        color: colors.light,
    },
    textContainer: {
        flex: 0.8,
        flexWrap: "nowrap",
        height: '100%',
        width: '100%',
    },
    input: {
        backgroundColor: colors.light,
    },
    link: {
        backgroundColor: colors.dark,
        height: 50,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    linkText: {
        color: colors.black,
        fontSize: 20,
    },
});


export default {
    styles,
    colors,
};