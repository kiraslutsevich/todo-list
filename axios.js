import axios from "axios";

const getPokemons = (params) =>  {
    const { limit, offset } = params;
    return axios.get(`https://pokeapi.co/api/v2/pokemon?limit=${limit}&offset=${offset}`);
};

export default getPokemons;