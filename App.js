import React from 'react';
import { NavigationContainer, Image } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import ListNav from './screen/ListScreen'
import HomeScreen from './screen/HomeScreen';
import PokemonsScreen from './screen/PokemonsScreen';

const MainStack = createBottomTabNavigator();

const App = () => {
  return (
      <NavigationContainer>
      <MainStack.Navigator initialRouteName="Home">
          <MainStack.Screen name="Todo-list" component={ListNav} />
          <MainStack.Screen name="Home" component={HomeScreen} />
          <MainStack.Screen name="Pokemons" component={PokemonsScreen} />
        </MainStack.Navigator>
      </NavigationContainer>
  );
}

export default App;
