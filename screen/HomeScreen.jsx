import React from "react"
import { Text, TouchableHighlight, ScrollView } from 'react-native';
import styles from "../styles";

const HomeScreen = ({navigation}) => {
    return (
        <ScrollView>
            <TouchableHighlight
            activeOpacity={0.6}
            underlayColor={styles.colors.light}
            onPress={() => navigation.navigate('Todo-list')}
            style={styles.styles.link}
            >
                <Text style={styles.styles.linkText}>Todo-List</Text>
            </TouchableHighlight>
            <TouchableHighlight
            activeOpacity={0.6}
            underlayColor={styles.colors.light}
            onPress={() => navigation.navigate('Pokemons')}
            style={styles.styles.link}
            >
                <Text style={styles.styles.linkText}>Pokemons</Text>
            </TouchableHighlight>
        </ScrollView>
    )
};

export default HomeScreen;