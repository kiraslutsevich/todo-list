import React from "react"
import { View, Text, FlatList, SafeAreaView, ActivityIndicator } from 'react-native';
import getPokemons from "../axios";
import Pokemon from "../components/Pokemon";
import styles from "../styles";


const PokemonsScreen = ({navigation}) => {
    const [pokemons, setPokemons] = React.useState([]);
    const renderItem = ({ item, i }) => 
        (
        <Pokemon 
            title={item.name} 
            navigation={navigation}
            id={i}
        /> 
        );

    React.useEffect(() => {
        (async () => {
            try {
                const response = await getPokemons({ limit: 5, offset: 0 });
                setPokemons(response.data.results);
            } catch (err) {
                console.log(err)
            } 
        })();
    }, []);

    React.useEffect(() => {
        console.log(pokemons);
    }, [pokemons])

    return (
        <SafeAreaView style={styles.styles.container}>
            { (pokemons.length > 0) ?
                <FlatList
                data={pokemons}
                renderItem={renderItem}
                keyExtractor={(item) => item.name}
                style={styles.styles.list}
                />
                : <ActivityIndicator size="large" color="#00ff00" />
            }
        </SafeAreaView>
    )
};

export default PokemonsScreen;