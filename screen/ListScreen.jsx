import React, { useState } from 'react';
import { FlatList, SafeAreaView, TextInput } from 'react-native';
import  styles  from '../styles';
import { todos } from '../todos';
import Task from '../components/Task';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import TaskScreen from './TaskScreen';

const ListStack = createNativeStackNavigator();

const ListScreen = ({ navigation }) => {
  const [tasks, setTasks] = useState(todos);
  const [text, onChangeText] = useState('');

  const deleteTask = (id) => {
    const filteredTasks = tasks.filter(task => task.id !== id);
    setTasks(filteredTasks);
  };

  const renderItem = ({ item }) => (
    <Task title={item.title} id={item.id} deleteTask={deleteTask} navigation={navigation}/>
  );

  const onSubmit = () => {
    const newTask = { title: text, id: Date.now().toString() };
    const updatedTasks = [...tasks, newTask];
    setTasks(updatedTasks);
    onChangeText('');
  };

  return (
    <SafeAreaView style={styles.styles.container}>
        <FlatList
          data={tasks}
          renderItem={renderItem}
          keyExtractor={task => task.id}
          style={styles.styles.list}
        />
        <TextInput
          style={styles.styles.input}
          onChangeText={onChangeText}
          onSubmitEditing={onSubmit}
          value={text}
        />
    </SafeAreaView>
  )
};

const ListNav = () => {
  return (
    <ListStack.Navigator>
      <ListStack.Screen name="List" component={ListScreen} />
      <ListStack.Screen name="Task" component={TaskScreen} />
    </ListStack.Navigator>
  )
}

export default ListNav;