import React from "react"
import { View, Text } from 'react-native';

const TaskScreen = ({route}) => {
    const text = JSON.stringify(route.params.title)
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>{text}</Text>
        </View>
    )
};

export default TaskScreen;